﻿// ref: https://weblog.west-wind.com/posts/2016/Jun/29/First-Steps-Exploring-NET-Core-and-ASPNET-Core 

using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseKestrel()
                .UseStartup<Startup>()
                .Build();
            host.Run();
        }
    }

    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }
        public void Configure(IApplicationBuilder app)
        {
            // app.Use(async (context, next) => 
            // {
            //     await context.Response.WriteAsync("Pre Processing");
            //     await next();
            //     await context.Response.WriteAsync("Post Processing");
            // });

            // app.Run(async (context) =>
            // {
            //     await context.Response.WriteAsync(
            //         "Hello World. The Time is: " + DateTime.Now.ToString("hh:mm:ss tt"));
            // });

            app.UseMvc();
        }
    }

    public class HelloWorldController : Controller
    {
        // API method
        [HttpGet("api/helloworld")]
        public object HelloWorld()
        {
            return new
            {
                message = "Hello World",
                time = DateTime.Now
            };
        }

        // MVC method
        [HttpGet("helloworld")]
        public ActionResult HelloworldMvc()
        {
            ViewBag.Message = "Hello world!";
            ViewBag.Time = DateTime.Now;

            // return View("helloworld");
            return View("~/helloworld.cshtml");
        }
    }


}
